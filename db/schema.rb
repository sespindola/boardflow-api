# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150829143332) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "projects", force: :cascade do |t|
    t.text     "title"
    t.text     "project_url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  create_table "sprints", force: :cascade do |t|
    t.text     "description"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer  "project_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "sprints", ["project_id"], name: "index_sprints_on_project_id", using: :btree
  add_index "sprints", ["user_id"], name: "index_sprints_on_user_id", using: :btree

  create_table "stories", force: :cascade do |t|
    t.text     "description"
    t.integer  "sprint_id"
    t.text     "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "stories", ["sprint_id"], name: "index_stories_on_sprint_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.text     "descriptiont"
    t.integer  "story_id"
    t.integer  "user_id"
    t.datetime "due_at"
    t.integer  "priority"
    t.text     "status"
    t.text     "commit_url"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "tasks", ["story_id"], name: "index_tasks_on_story_id", using: :btree
  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.text     "username"
    t.text     "email"
    t.text     "role"
    t.text     "fullname"
    t.text     "password_hash"
    t.boolean  "disabled",      default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_foreign_key "sprints", "projects"
  add_foreign_key "sprints", "users"
  add_foreign_key "stories", "sprints"
  add_foreign_key "tasks", "stories"
  add_foreign_key "tasks", "users"
end
