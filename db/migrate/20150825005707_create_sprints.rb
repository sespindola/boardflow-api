class CreateSprints < ActiveRecord::Migration
  def change
    create_table :sprints do |t|
      t.text :description
      t.timestamp :starts_at
      t.timestamp :ends_at
      t.references :project, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
