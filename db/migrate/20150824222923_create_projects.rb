class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.text :title
      t.text :project_url

      t.timestamps null: false
    end
  end
end
