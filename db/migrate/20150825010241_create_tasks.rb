class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :descriptiont
      t.references :story, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.timestamp :due_at
      t.integer :priority
      t.text :status
      t.text :commit_url

      t.timestamps null: false
    end
  end
end
