class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.text :username
      t.text :email
      t.text :role
      t.text :fullname
      t.text :password_hash
      t.boolean :disabled, default: false
      

      t.timestamps null: false
    end
  end
end
