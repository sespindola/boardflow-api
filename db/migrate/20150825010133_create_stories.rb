class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.text :description
      t.references :sprint, index: true, foreign_key: true
      t.text :status

      t.timestamps null: false
    end
  end
end
