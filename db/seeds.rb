# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

gh = Github.new basic_auth: 'sespindola:1rustno1'
gh.repos.list.each do |repo|
  proj = Project.new
  proj.title = repo.name
  proj.description = repo.description
  proj.project_url = repo.clone_url
  proj.save
end
