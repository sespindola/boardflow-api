class Sprint < ActiveRecord::Base
  belongs_to :project
  belongs_to :master
  has_many :stories
end
